import db from '../config/database';
import bcrypt from 'bcryptjs';

/**
 * Modelo a ser acionado para adicionar uma credencial para o usuário
 * 
 * @param {String} email 
 * @param {String} password 
 */
export function MDLnewLogin(email, password) {

    const query = `INSERT INTO LoginUser(email_user,password_user) VALUES ("${email}","${password}")`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });
}

/**
 * Modelo a ser acionado para verificar se o usuário esta confirmado
 * 
 * @param {Integer} id 
 */
export function MDLverifyUser(idLoginUser){
    const query = `SELECT idUser FROM User WHERE idLoginUser = "${idLoginUser}"`;

    return new Promise( (resolve, reject) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            if (result.lenght != 1)
                reject(null);
            else
                resolve(result);
        });
    });    
}

/**
 * Modelo a ser acionado para adicionar informações de um usuário
 * 
 * @param {String} first_name 
 * @param {String} last_name 
 * @param {String} description 
 * @param {String} typeUser 
 * @param {String} idStateUser 
 */
export function MDLnewData(first_name,last_name,description){
    const query = `INSERT INTO DetailsUser(first_name_user,last_name_user,description_user,idTypeUser) VALUES ("${first_name}","${last_name}","${description}", 3)`

    return new Promise( (resolve, reject,fields) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result.insertId);
        });
    });    
}

/**
 * Modelo a ser acionado para adicionar um novo usuario
 * 
 * @param {*} idDetailsUser 
 * @param {*} idLoginUser 
 */
export function MDLnewUser(idDetailsUser,idLoginUser){
    const query = `INSERT INTO User(idDetailsUser,idLoginUser,idAccessStateUser) VALUES ("${idDetailsUser}","${idLoginUser}",1)`

    return new Promise( (resolve, reject,fields) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });       
}

/**
 * Modelo a ser acionado para adicionar um novo numero de telefone do usuario
 * 
 * @param {*} numberPhone 
 * @param {*} idPhoneDetails 
 */
export function MDLnewPhoneUser(numberPhone,idPhoneDetails){
    const query = `INSERT INTO User(number_phone,idPhoneDetails,idTypePhone) VALUES ("${numberPhone}","1",1)`

    return new Promise( (resolve, reject,fields) => {
        db.query(query, (err, result) => {
            if (err)
                reject(err);
            else
                resolve(result);
        });
    });     
}