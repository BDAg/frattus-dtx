import db from '../config/database';

/**
 * Essa Classe busca os dados do usuário para realizar a autenticação.
 */
class AuthModel {
  static findUser(email) {
    const query = `SELECT * FROM LoginUser WHERE email_user = "${email}"`;

    return new Promise((resolve, reject) => {
      db.query(query, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result[0]);
        }
      });
    });
  }

  /**
   * Essa classe busca os dados do usuário para realizar a verificação de acesso
   * 
   * @param {*} id 
   */
  static selectUser(id){
    const query = `SELECT * FROM User WHERE idLoginUser = "${id}"`;

    return new Promise((resolve, reject) => {
      db.query(query, (err, result) => {
        if (err) {
          reject(err);
        } else {
          resolve(result[0]);
        }
      });
    });

  }

}



export default AuthModel;
