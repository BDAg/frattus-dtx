import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'cardapio', loadChildren: './pages/cardapio/cardapio.module#CardapioPageModule' },
  { path: 'opcoes-cardapio1', loadChildren: './pages/opcoes-cardapio1/opcoes-cardapio1.module#OpcoesCardapio1PageModule' },
  { path: 'lista-cardapio', loadChildren: './pages/lista-cardapio/lista-cardapio.module#ListaCardapioPageModule' },
  { path: 'ficha', loadChildren: './pages/ficha/ficha.module#FichaPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'cadastro', loadChildren: './pages/cadastro/cadastro.module#CadastroPageModule' },
  { path: 'metas-usuario', loadChildren: './pages/metas-usuario/metas-usuario.module#MetasUsuarioPageModule' },
  { path: 'home', loadChildren: './tabs/tabs.module#TabsPageModule'},  { path: 'blank', loadChildren: './blank/blank.module#BlankPageModule' },
  { path: 'success-view', loadChildren: './success-view/success-view.module#SuccessViewPageModule' }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
