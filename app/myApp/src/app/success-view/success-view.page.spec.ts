import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessViewPage } from './success-view.page';

describe('SuccessViewPage', () => {
  let component: SuccessViewPage;
  let fixture: ComponentFixture<SuccessViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessViewPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
