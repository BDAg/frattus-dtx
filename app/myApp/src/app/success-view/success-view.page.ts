import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-success-view',
  templateUrl: './success-view.page.html',
  styleUrls: ['./success-view.page.scss'],
})
export class SuccessViewPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  toLogin(){
    this.navCtrl.navigateForward('login');
  }

  ngOnInit() {
  }

}
