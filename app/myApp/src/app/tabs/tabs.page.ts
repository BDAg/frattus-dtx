import { Component } from '@angular/core';
import { SessionService } from '../session.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(public session: SessionService, public navCtrl: NavController) {}

  ionViewWillEnter(){
    this.session.get().then((e) => {
      if(!e){
        this.navCtrl.navigateForward('login');
      }
    })
  }


}
