import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../session.service';
import $ from "jquery";
import axios from 'axios';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {


  constructor(public session: SessionService,public navCtrl: NavController) { }

  public mail;
  public passwd;

  signIn(){
    this.navCtrl.navigateForward('cadastro');
  }

  logIn(){

    $(document).on("click", ".login__submit", (e) =>  {
      var that = e.target;
      var these = this;
      $(that).addClass("processing");

      axios({
        method: 'post',
        url: 'http://localhost:8080/v1/auth',
        data: {
          email:'leandro.salustriano@hotmail.com',//these.mail,//mail,
          password: '97975037123Le!'//these.passwd//passwd
        }
      }).then(function (response) {
        these.session.create(response.data.token)
        setTimeout(function() {
          $(that).addClass("success");
          setTimeout(function() {
            $(that).removeClass("success processing");
          }, 550);
          setTimeout(function() {
            these.navCtrl.navigateForward('home');
          }, 250)          
        }, 1100);
      }).catch(function (error) {
        setTimeout(() => {
          console.log(error)
          $(that).removeClass("success processing");
        }, 1100)
        
      });
     
    });
  
  }

  ngOnInit() {
    this.session.get().then((e) => {
      if(e){
        this.session.remove()
      }
    })
  }

}
