import { Component, OnInit } from '@angular/core';
import $ from "jquery";
import axios from 'axios';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  constructor(public navCtrl: NavController) { }

  public user = {};

  toLogin(){
    this.navCtrl.navigateForward('login');
  }

  next(){
    
    let these = this;
    $(document).on("click", ".login__submit", (u) => {
      console.log("q")
      let that = u.target
      if(!these.user["mail"]){
        these.user["mail"] = $('input').val();
      } else {
        these.user["pass"] = $('input').val();
      }
      $('path').attr('d','M0,20 20,20 20,8 0,8z M10,13 10,16z M4,8 a6,8 0 0,1 12,0');
      $('input').attr('placeholder',"Digite sua senha")
      $('button').html("Finalizar cadastro")
      $('input').val('')
      // new way (jQuery 1.7+) - off(events);
      $('button').off('click');
      $('input').attr("type","password")
      if (these.user["mail"] && these.user["pass"]){
        $(that).addClass("processing");

        axios({
          method: 'post',
          url: 'http://localhost:8080/v1/users',
          data: {
            email:these.user["mail"],//mail,
            password: these.user["pass"]//these.passwd//passwd
          }
        }).then(function (response) {
          setTimeout(() => {
            $(that).removeClass("success processing");
            these.navCtrl.navigateForward('success-view');
          }, 1100)          
        }).catch(function (error) {
          setTimeout(() => {
            console.log(error)
            $(that).removeClass("success processing");            
          }, 1100)
          
        });

      }

      
    })

  }

  ngOnInit() {
  }

}
