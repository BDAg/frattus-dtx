import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCardapioPage } from './lista-cardapio.page';

describe('ListaCardapioPage', () => {
  let component: ListaCardapioPage;
  let fixture: ComponentFixture<ListaCardapioPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaCardapioPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaCardapioPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
