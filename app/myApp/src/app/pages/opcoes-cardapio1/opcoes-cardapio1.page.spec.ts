import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpcoesCardapio1Page } from './opcoes-cardapio1.page';

describe('OpcoesCardapio1Page', () => {
  let component: OpcoesCardapio1Page;
  let fixture: ComponentFixture<OpcoesCardapio1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpcoesCardapio1Page ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpcoesCardapio1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
