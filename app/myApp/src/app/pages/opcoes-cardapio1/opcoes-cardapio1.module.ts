import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { OpcoesCardapio1Page } from './opcoes-cardapio1.page';

const routes: Routes = [
  {
    path: '',
    component: OpcoesCardapio1Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [OpcoesCardapio1Page]
})
export class OpcoesCardapio1PageModule {}
