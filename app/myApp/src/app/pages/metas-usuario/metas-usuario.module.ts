import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { MetasUsuarioPage } from './metas-usuario.page';

const routes: Routes = [
  {
    path: '',
    component: MetasUsuarioPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MetasUsuarioPage]
})
export class MetasUsuarioPageModule {}
