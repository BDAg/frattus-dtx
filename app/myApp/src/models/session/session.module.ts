import { Storage } from "@ionic/storage";
import { Injectable } from '@angular/core';

@Injectable()
export class Session {
	constructor(public storage: Storage){}
	
	create(SESS:String){
		this.storage.set("SESSIONID",SESS);
	}
	
    get(): Promise<any> {
        return this.storage.get('SESSIONID');
    }
	
    remove() {
        this.storage.remove('SESSIONID');
    }	
	
}