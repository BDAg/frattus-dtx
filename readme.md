# FRATTU$

~~~
Um projeto com intuito de auxiliar as pessoas a terem uma qualidade de vida melhor 
com base em uma alimentação mais saudável.
Através da utilização das mais atuais tecnologias esperamos contribuir com a população.
~~~ 

# Documentação
<br>[Matriz de Habilidade](https://gitlab.com/BDAg/frattus-dtx/wikis/Matriz-de-Habilidade)</br>
<br>[Matriz de Habilidade - Final](https://gitlab.com/BDAg/frattus-dtx/wikis/Matriz-de-Habilidade---Final)</br>
<br>[Mapa de Conhecimento](https://gitlab.com/BDAg/frattus-dtx/wikis/Mapa-de-conhecimento)</br>
<br>[Cronograma](https://docs.google.com/spreadsheets/d/1ySGvJQqSUiywglK6CMlgM4NpyrQ0-SWeKvTQbxOqrEI/edit#gid=0)</br>
<br>[MVP](https://gitlab.com/BDAg/frattus-dtx/wikis/MVP)</br>
<br>[Documentação de Feedback](https://gitlab.com/BDAg/frattus-dtx/wikis/Documentação-de-Feedback)</br>
<br>[Lições Aprendidas](https://gitlab.com/BDAg/frattus-dtx/wikis/Lições-Aprendidas)</br>

